package modulos;

import singleton.Notificador;
import singleton.Singleton;

public class ModuloVenta {
    public void enviarNotificacion(String mensaje) {

        Notificador notificador = Singleton.getInstance();
        notificador.setMensaje(mensaje);
        notificador.enviar();
    }
}
