package com.qaraywa.controlador;

import com.qaraywa.dto.Operacion;
import com.qaraywa.presentador.QaraywaPresentador;
import com.qaraywa.servicio.impl.SingletonServicio;

public class QaraywaControlador {
    public static void main(String[] args) {
        Operacion operacion1 = new Operacion(100.50,"I","Venta artículo");
        SingletonServicio.getInstance().agregarOperacion(operacion1);
        Operacion operacion2 = new Operacion(7.50,"E","Compra de cuaderno");
        SingletonServicio.getInstance().agregarOperacion(operacion2);
        QaraywaPresentador.imprimir(SingletonServicio.getInstance().obtenerOperaciones());
    }
}
