package com.qaraywa.presentador;

import com.qaraywa.dto.Operacion;

import java.util.List;

public class QaraywaPresentador {
    public static void imprimir(List<Operacion> operaciones) {
        String respuesta = "";
        respuesta = "Tipo\tDescripcion\t\tMonto\n";
        for (int i = 0; i < operaciones.size(); i++) {
            respuesta = respuesta +
                    operaciones.get(i).getTipo() + "\t" +
                    operaciones.get(i).getDescripcion() + "\t" +
                    operaciones.get(i).getMonto() + "\n";
        }
        System.out.println(respuesta);
    }
}
