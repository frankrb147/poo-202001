package com.qaraywa.servicio.impl;

import com.qaraywa.dto.Operacion;
import com.qaraywa.servicio.LibroDiarioServicio;

import java.util.ArrayList;
import java.util.List;

public class LibroDiarioServicioImpl extends LibroDiarioServicio {
    public boolean agregarOperacion(Operacion operacion){
        boolean respuesta = false;
        if(this.operaciones == null){
            this.operaciones = new ArrayList<Operacion>();
        }
        this.operaciones.add(operacion);
        respuesta = true;
        return respuesta;
    }
    public List<Operacion> obtenerOperaciones(){
        return this.operaciones;
    }
}
