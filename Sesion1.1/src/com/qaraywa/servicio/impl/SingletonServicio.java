package com.qaraywa.servicio.impl;


public class SingletonServicio {
    private static LibroDiarioServicioImpl servicio = null;
    public static LibroDiarioServicioImpl getInstance(){
        if(servicio == null){
            servicio = new LibroDiarioServicioImpl();
        }
        return servicio;
    }
}
