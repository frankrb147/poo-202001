package com.qaraywa.servicio;

import com.qaraywa.dto.Operacion;
import java.util.List;

public abstract class LibroDiarioServicio {
    protected List<Operacion> operaciones;

    public abstract boolean agregarOperacion(Operacion operacion);
    public abstract List<Operacion> obtenerOperaciones();
}
