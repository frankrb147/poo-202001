package com.qaraywa.dto;

public class Operacion {
    private Double monto;
    private String tipo; // I: Ingreso, E: Egreso
    private String descripcion;
    public Operacion(Double monto, String tipo, String descripcion) {
        this.monto = monto;
        this.descripcion = descripcion;
        if("I".equals(tipo)){
            this.tipo = "Ingreso";
        }
        else if("E".equals(tipo)){
            this.tipo = "Egreso";
        }
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
