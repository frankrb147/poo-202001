package estatico;

public class Consumidor {
    public static void main(String[] args) {
        //Ejemplo e = new Ejemplo();
        //e.setEdad(10);
        Ejemplo.nombre="Rony";
        Consumidor c = new Consumidor();
        c.imprimir("valor2");
        c.imprimir(0);
        c.imprimir(0,2);
        c.imprimir(0,2,5);
        c.imprimir(0,2,5,6);
        c.imprimir();

        Anidada a = new Anidada();
        a.leer();
    }
    public void imprimir(){
        System.out.println("sin argumentos");
    }
    public void imprimir(Integer e){
        System.out.println("un entero");
    }

    public void imprimir(String e){
        System.out.println("string");
    }

    public void imprimir(Integer e, Integer z){
        System.out.println("dos enteros");
    }
    public void imprimir(Integer ... e){
        System.out.println("vararg");
    }

}
