package estatico;

public class Ejemplo {
    private Integer edad;//variable de instancia
    public static String nombre;//atributo estatica

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        Ejemplo.nombre = nombre;
    }
}
