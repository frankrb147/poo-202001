package estatico;

public class Anidada {//outer class
    private String nombre;
    public void leer(){
        Anidada1 a = new Anidada1();
        a.imprimir();
    }
    private class Anidada1{
        private String descripcion;

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
        public void imprimir(){

        }
    }
}
