package singleton;

public class Notificador {
    private String mensaje;
    private int contador;

    Notificador(){
        this.mensaje="";
        this.contador=0;
    }
    public Notificador getInstance(){
        return new Notificador();
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public void enviar(){
        System.out.println("Mensaje"+contador+ ": "+ mensaje);
    }
}
