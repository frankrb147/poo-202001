package pe.edu.uni.fiis.atm.controller;

import pe.edu.uni.fiis.atm.dto.Cuenta;
import pe.edu.uni.fiis.atm.dto.Tarjeta;
import pe.edu.uni.fiis.atm.servicio.Operable;
import pe.edu.uni.fiis.atm.servicio.OperableImpl;
import pe.edu.uni.fiis.atm.servicio.SingletonOperable;

import java.util.ArrayList;

public class Controlador {
    public static void main(String[] args) {
        Cuenta rony = new Cuenta();
        rony.setSaldo(1000.50);
        rony.setClave("1679");
        rony.setCodigo("046794124");
        rony.setLista(new ArrayList<>());
        rony.setMoneda("SOLES");
        Tarjeta tarjetaRony = new Tarjeta();
        tarjetaRony.setTipo("Debito");
        tarjetaRony.setNumero("4261-7892-1679-4892");
        rony.setTarjeta(tarjetaRony);

        Operable operable = SingletonOperable.getInstance();
        operable.revisarSaldo(rony);

        operable.retirarEfectivo(rony,120.50);

        Cuenta jack = new Cuenta();
        jack.setSaldo(999500.50);
        jack.setClave("0597");
        jack.setCodigo("046794175");
        jack.setLista(new ArrayList<>());
        jack.setMoneda("SOLES");
        Tarjeta tarjetajack = new Tarjeta();
        tarjetajack.setTipo("Credito");
        tarjetajack.setNumero("4200-7892-1679-7981");
        jack.setTarjeta(tarjetajack);
        operable.revisarSaldo(jack);

        operable.transferir(rony,jack,250.50);
        System.out.println("Cuenta Rony");
        operable.revisarSaldo(rony);

        System.out.println("Cuenta Jack");
        operable.revisarSaldo(jack);
    }
}
