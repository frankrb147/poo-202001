package pe.edu.uni.fiis.atm.dto;

import java.util.List;

public class Cuenta {
    private Double saldo;
    private String codigo;
    private String clave;
    private String moneda;
    private List<MovimientoCuenta> lista;
    private Tarjeta tarjeta;

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public List<MovimientoCuenta> getLista() {
        return lista;
    }

    public void setLista(List<MovimientoCuenta> lista) {
        this.lista = lista;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String toString() {
        String mensaje = "Cuenta:"+getCodigo() +"\n" +
            "Tarjeta:"+getTarjeta().getNumero()+"\n" +
            "Moneda:"+getMoneda()+"\n" +
            "Saldo:"+getSaldo();
        return mensaje;
    }
}
