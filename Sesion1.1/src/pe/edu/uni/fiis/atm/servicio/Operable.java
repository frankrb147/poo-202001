package pe.edu.uni.fiis.atm.servicio;

import pe.edu.uni.fiis.atm.dto.Cuenta;

public interface Operable {
    public void retirarEfectivo(Cuenta cuenta,Double monto);
    public void transferir(Cuenta cuentaOrigen,Cuenta cuentaDestino, Double monto);
    public void revisarSaldo(Cuenta cuenta);
}
