package pe.edu.uni.fiis.enumeraciones;

public class Principal {
    public static void main(String[] args) {
        Especialidad sistemas = Especialidad.SISTEMAS;
        System.out.println(sistemas.getCodigo());
        System.out.println(sistemas.getAlumnos());

        Especialidad industrial = Especialidad.INDUSTRIAL;
        System.out.println(industrial.getCodigo());
        System.out.println(industrial.getAlumnos());

        for (Especialidad e:Especialidad.values()) {
            System.out.println(e.getCodigo());
            System.out.println(e.getNombre());
        }
        for (TipoCurso e:TipoCurso.values()) {
            System.out.println(e);
        }
    }
}
