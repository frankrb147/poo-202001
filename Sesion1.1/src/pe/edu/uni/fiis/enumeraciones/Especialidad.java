package pe.edu.uni.fiis.enumeraciones;

public enum Especialidad {
    SISTEMAS("Ingeniería de Sistemas",
            "I2",2000),
    INDUSTRIAL("Ingeniería Industrial",
            "I1",2500);

    private Especialidad(String nombre, String codigo, Integer alumnos) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.alumnos = alumnos;
    }

    private String nombre;
    private String codigo;
    private Integer alumnos;

    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public Integer getAlumnos() {
        return alumnos;
    }
}
