package pe.edu.uni.fiis.patrones;

public class Celular implements Telefonoable{
    private Reset reset;

    public Reset getReset() {
        return reset;
    }

    public void setReset(Reset reset) {
        this.reset = reset;
    }
    public void iniciarRadioFM(){
    }
    public void aplicarAplicacion(){
    }


    public void llamar() {

    }

    public void enviarMensaje() {

    }

    public void enviarNotificaciones() {

    }

    public void leerBuzon() {

    }

    public void leerHistorial() {

    }

    public void subirVolumen() {

    }

    public void almacenarAgenda() {

    }
}
