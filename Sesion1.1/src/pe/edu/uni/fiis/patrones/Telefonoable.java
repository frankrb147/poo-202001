package pe.edu.uni.fiis.patrones;

public interface Telefonoable {
    public abstract void llamar();
    public abstract void enviarMensaje();
    public abstract void enviarNotificaciones();
    public abstract void leerBuzon();
    public abstract void leerHistorial();
    public abstract void subirVolumen();
    public abstract void almacenarAgenda();
}
