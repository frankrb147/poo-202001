package calculadora.presentacion;

import calculadora.dto.Operacion;
import calculadora.servicio.Calculadora;
import calculadora.servicio.CalculadoraBasica;
import calculadora.servicio.CalculadoraCientifica;

public class Presentacion {
    public void imprimir(Operacion operacion, CalculadoraBasica calculadora){
        System.out.println("La operación es:");
        System.out.println(operacion.getPrimero()
            + " "+ operacion.getOperador()+ " "+ operacion.getSegundo()
            + " = " + calculadora.operar(operacion)
        );

    }

    public static void main(String[] args) {
        Presentacion p = new Presentacion();
        Operacion operacion = new Operacion();
        operacion.setPrimero(4.0);
        operacion.setSegundo(2d);
        operacion.setOperador("*");
        //CalculadoraBasica calculadora = new Calculadora();
        CalculadoraBasica calculadora = new CalculadoraCientifica();
        p.imprimir(operacion,calculadora);
    }
}
