package calculadora.servicio;

import calculadora.dto.Operacion;

public class Calculadora extends CalculadoraBasica{

    public Double operar(Operacion operacion){
        if("+".equals(operacion.getOperador())){
            return sumar(operacion);
        }
        else if("-".equals(operacion.getOperador())){
            return restar(operacion);
        }
        else if("*".equals(operacion.getOperador())){
            return multiplicar(operacion);
        }
        else if("/".equals(operacion.getOperador())){
            return dividir(operacion);
        }
        else{
            return 0d;
        }
    }

}
