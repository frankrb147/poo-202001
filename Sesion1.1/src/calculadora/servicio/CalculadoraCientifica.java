package calculadora.servicio;

import calculadora.dto.Operacion;

public class CalculadoraCientifica extends CalculadoraBasica{
    public Double operar(Operacion operacion) {
        if("+".equals(operacion.getOperador())){
            return sumar(operacion);
        }
        else if("-".equals(operacion.getOperador())){
            return restar(operacion);
        }
        else if("*".equals(operacion.getOperador())){
            return multiplicar(operacion);
        }
        else if("/".equals(operacion.getOperador())){
            return dividir(operacion);
        }
        else if("^".equals(operacion.getOperador())){
            return potenciar(operacion);
        }
        else{
            return 0d;
        }
    }
    public Double potenciar(Operacion operacion){
        return Math.pow(operacion.getPrimero(),operacion.getSegundo());
    }
}
