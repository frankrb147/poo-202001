package calculadora.servicio;

import calculadora.dto.Operacion;

public abstract class CalculadoraBasica {
    public abstract Double operar(Operacion operacion);
    public Double sumar(Operacion operacion){
        return operacion.getPrimero()
                + operacion.getSegundo();
    }
    public Double restar(Operacion operacion){
        return operacion.getPrimero()
                - operacion.getSegundo();
    }
    public Double multiplicar(Operacion operacion){
        return operacion.getPrimero()
                * operacion.getSegundo();
    }
    public Double dividir(Operacion operacion){
        return operacion.getPrimero()
                / operacion.getSegundo();
    }
}
