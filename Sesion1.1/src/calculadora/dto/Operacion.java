package calculadora.dto;

public class Operacion {
    private Double primero;
    private Double segundo;
    private String operador;

    public Double getPrimero() {
        return primero;
    }

    public void setPrimero(Double primero) {
        this.primero = primero;
    }

    public Double getSegundo() {
        return segundo;
    }

    public void setSegundo(Double segundo) {
        this.segundo = segundo;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }
}
