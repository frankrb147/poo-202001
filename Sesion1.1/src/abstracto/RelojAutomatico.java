package abstracto;

public class RelojAutomatico extends Reloj{
    private int energia;

    public RelojAutomatico() {
        super(200);
        System.out.println("RelojAutomatico sin");
        this.energia = 100;
    }

    public void incrementar() {
        if(this.energia>0){
            this.setInicio(this.getInicio()+1);
            this.energia--;
        }
        else{
            this.setInicio(0);
        }
    }
    public void incrementarEnergia(){
        this.energia = this.energia + 1;
    }

    public int getEnergia() {
        return energia;
    }

    public void setEnergia(int energia) {
        this.energia = energia;
    }
}
