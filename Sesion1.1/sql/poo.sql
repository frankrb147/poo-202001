create database poo;
use poo;

create table alumno(
	codigo	varchar(10),
	nombres	varchar(100),
	apellido_paterno	varchar(50),
	apellido_materno varchar(50)
);

select codigo,nombres,apellido_paterno,apellido_materno
from alumno;

insert into alumno(codigo,nombres,apellido_paterno,apellido_materno)
values('20202030','Rodolfo','Torres','Valencia');

commit;

update alumno
set nombres = 'Roberto'
where codigo='20202030';

delete from alumno
where codigo='20202030';


select codigo codigo_universitario,nombres,apellido_paterno,apellido_materno
from alumno
where apellido_materno like '%e%'
order by nombres desc;


select codigo codigo_universitario,nombres,apellido_paterno,apellido_materno
from alumno
where codigo between '20202026' and '20202035'
order by nombres desc;
